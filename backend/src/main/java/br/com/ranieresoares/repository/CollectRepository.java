package br.com.ranieresoares.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ranieresoares.entity.CollectEntity;

@Repository
public interface CollectRepository extends JpaRepository<CollectEntity, Long>{

}
