package br.com.ranieresoares.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ranieresoares.entity.GasStationEntity;

@Repository
public interface GasStationRepository extends JpaRepository<GasStationEntity, Integer>{

}
