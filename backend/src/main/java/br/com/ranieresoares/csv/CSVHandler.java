package br.com.ranieresoares.csv;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.ranieresoares.entity.AddressEntity;
import br.com.ranieresoares.entity.CollectEntity;
import br.com.ranieresoares.entity.GasStationEntity;

public class CSVHandler {
	
	public static CollectEntity buildCollectEntity(String[] fields) throws ParseException {
		AddressEntity address = new AddressEntity();
		address.setRegion(fields[0].trim());
		address.setCountryState(fields[1].trim());
		address.setCity(fields[2].trim());
		
		GasStationEntity gasStation = new GasStationEntity();
		gasStation.setResale(fields[3].trim());
		gasStation.setInstall(new Integer(fields[4].trim()));
		gasStation.setBrand(fields[10].trim());
		gasStation.setAdress(address);
		
		CollectEntity collect = new CollectEntity();
		collect.setProduct(fields[5].trim());
		collect.setCollectDate(new SimpleDateFormat("dd/MM/yyyy").parse(fields[6].trim()));
		collect.setBuyingPrice(fixFloatValue(fields[7].trim()));
		collect.setSellingPrice(fixFloatValue(fields[8].trim()));
		collect.setUnit(fields[9].trim());
		collect.setGasStation(gasStation);
		
		return collect;
	}
	
	private static Float fixFloatValue(String value) {
		value = value.replace(',', '.');
		return value.equals("") ? new Float(0) : new Float(value);
	}
}
