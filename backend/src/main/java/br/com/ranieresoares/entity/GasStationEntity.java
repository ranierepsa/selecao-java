package br.com.ranieresoares.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbl_gas_station")
@Getter @Setter
public class GasStationEntity {
	
	@Id
	@Column(name = "install", nullable = false)
	private Integer install;
	
	@Column(name = "resale", nullable = false)
	private String resale;
	
	@Column(name = "brand", nullable = false)
	private String brand;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id", nullable = false)
	private AddressEntity address;

	public AddressEntity getAdress() {
		return address;
	}

	public void setAdress(AddressEntity adress) {
		this.address = adress;
	}
	
}
