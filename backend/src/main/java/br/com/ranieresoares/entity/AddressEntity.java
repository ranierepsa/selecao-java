package br.com.ranieresoares.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbl_address")
@Getter @Setter 
public class AddressEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "region", nullable = false, length = 2)
	private String region;
	
	@Column(name = "country_state", nullable = false, length = 2)
	private String countryState;
	
	@Column(name = "city", nullable = false, length = 50)
	private String city;
	
}
