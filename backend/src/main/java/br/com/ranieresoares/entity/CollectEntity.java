package br.com.ranieresoares.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbl_collect")
@Getter @Setter
public class CollectEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "product", nullable = false)
	private String product;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "collect_date", nullable = false)
	private Date collectDate;
	
	@Column(name = "buy_price", precision = 8, scale = 2)
	private Float buyingPrice;
	
	@Column(name = "sell_price", precision = 8, scale = 2)
	private Float sellingPrice;
	
	@Column(name = "unit", nullable = false, length = 10)
	private String unit;
	
	@ManyToOne
	@JoinColumn(name = "gas_station_id", nullable = false)
	private GasStationEntity gasStation;
	
}
