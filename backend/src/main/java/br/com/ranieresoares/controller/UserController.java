package br.com.ranieresoares.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranieresoares.entity.UserEntity;
import br.com.ranieresoares.exception.RecordNotFoundException;
import br.com.ranieresoares.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	UserService service;
	
	private final String SUCCESS_DELETE = "Excluído com sucesso!";
	
	@GetMapping
	public ResponseEntity<List<UserEntity>> getAllUsers() {
		System.out.println("Pegando todos os usuarios");
		List<UserEntity> list = service.getAllUsers();
		
		return new ResponseEntity<List<UserEntity>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<UserEntity> getUserById(@PathVariable("id") Long id) {
		UserEntity result;
		try {
			result = service.getUserById(id);
			return new ResponseEntity<UserEntity>(result, new HttpHeaders(), HttpStatus.OK);
		} catch (RecordNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<UserEntity> createOrUpdateEntity(@RequestBody UserEntity entity) {
		UserEntity result = service.createOrUpdateUser(entity);
		
		return new ResponseEntity<UserEntity>(result, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping(path = "/authenticate", consumes = "application/json", produces = "application/json")
	public HttpStatus authenticate(@RequestBody UserEntity entity) {
		System.out.println("Tentou logar!");
		if (service.authenticate(entity)) {
			System.out.println("Logou!");
			return HttpStatus.OK;
		} else {
			System.out.println("Não logou !");
			return HttpStatus.UNAUTHORIZED;
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteUserById(@PathVariable("id") Long id) {
		try {
			service.deleteUserById(id);
			return ResponseEntity.ok(SUCCESS_DELETE);
		} catch (RecordNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}
}
