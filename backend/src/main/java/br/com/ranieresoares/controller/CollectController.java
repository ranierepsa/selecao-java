package br.com.ranieresoares.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranieresoares.entity.CollectEntity;
import br.com.ranieresoares.exception.RecordNotFoundException;
import br.com.ranieresoares.service.CollectService;

@RestController
@RequestMapping("/collects")
public class CollectController{
	
	@Autowired
	CollectService service;
	
	private final String SUCCESS_DELETE = "Excluído com sucesso!";
	
	@GetMapping
	public ResponseEntity<List<CollectEntity>> getAllCollects() {
		List<CollectEntity> list = service.getAllCollects();
		
		return new ResponseEntity<List<CollectEntity>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CollectEntity> getCollectById(@PathVariable("id") Long id) {
		CollectEntity result;
		try {
			result = service.getCollectById(id);
			return new ResponseEntity<CollectEntity>(result, new HttpHeaders(), HttpStatus.OK);
		} catch (RecordNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<CollectEntity> createOrUpdateEntity(@RequestBody CollectEntity entity) {
		CollectEntity result = service.createOrUpdateCollect(entity);
		return new ResponseEntity<CollectEntity>(result, new HttpHeaders(), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteCollectById(@PathVariable("id") Long id) {
		try {
			service.deleteCollectById(id);
			return ResponseEntity.ok(SUCCESS_DELETE);
		} catch (RecordNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}
	
	@GetMapping("/importFile")
	public ResponseEntity<String> importFile() {
		Thread thread = new Thread(){
			public void run(){
				service.importFile();
		    }
		};
		thread.start();
		
		return ResponseEntity.ok("Importação do arquivo foi iniciada");
	}
	
	@GetMapping("/getCollectsByRegion/{region}")
	public ResponseEntity<List<CollectEntity>> getCollectsByRegion(@PathVariable("region") String region) {
		List<CollectEntity> list = service.getCollectsByRegion(region);
		return new ResponseEntity<List<CollectEntity>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/getCollectsGroupByBrand")
	public ResponseEntity<List<CollectEntity>> getCollectsGroupByBrand(@RequestBody String brand) {
		List<CollectEntity> list = service.getCollectsGroupByBrand(brand);
		return new ResponseEntity<List<CollectEntity>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	
	@PostMapping("/getCollectsGroupByCollectDate")
	public ResponseEntity<List<CollectEntity>> getCollectsGroupByCollectDate(@RequestBody String date) throws ParseException{
		List<CollectEntity> list = service.getCollectsGroupByCollectDate(date);
		return new ResponseEntity<List<CollectEntity>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	@PostMapping("/getCityAvgSellingPrice")
	public ResponseEntity<Double> getCityAvgSellingPrice(@RequestBody String city){
		Double result = service.getCityAvgSellingPrice(city);
		return new ResponseEntity<Double>(result, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/getCitiesAvgBuyingSellingPrice")
	public ResponseEntity<List<Object>> getCitiesAvgBuyingSellingPrice(){
		List<Object> result = service.getCitiesAvgBuyingSellingPrice();
		return new ResponseEntity<List<Object>>(result, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/getBrandsAvgBuyingSellingPrice")
	public ResponseEntity<List<Object>> getBrandsAvgBuyingSellingPrice(){
		List<Object> result = service.getBrandsAvgBuyingSellingPrice();
		return new ResponseEntity<List<Object>>(result, new HttpHeaders(), HttpStatus.OK);
	}
}
