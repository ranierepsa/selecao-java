package br.com.ranieresoares.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieresoares.entity.UserEntity;
import br.com.ranieresoares.exception.RecordNotFoundException;
import br.com.ranieresoares.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<UserEntity> getAllUsers() {
		List<UserEntity> result = repository.findAll();
		
		if (result.size() > 0)
			return result;
		else
			return new ArrayList<UserEntity>();
	}
	
	public UserEntity getUserById(Long id) throws RecordNotFoundException {
		Optional<UserEntity> user = repository.findById(id);
		
		if (user.isPresent())
			return user.get();
		else 
			throw new RecordNotFoundException();
	}
	
	public UserEntity createOrUpdateUser(UserEntity entity) {
		if (entity.getId() != null && repository.findById(entity.getId()).isPresent()) {
			UserEntity newUser = repository.findById(entity.getId()).get();
			newUser.setName(entity.getName());
			newUser.setPassword(entity.getPassword());
			
			newUser = repository.save(newUser);
			return newUser;
			
		} else {
			if (entity.getId() != null) 
				entity.setId(null);
			return repository.save(entity);
		}
	}
	
	public Boolean authenticate(UserEntity entity) {
		String jpql = ""
				+ " SELECT user "
				+ " FROM UserEntity user "
				+ " WHERE user.name = :name AND user.password = :password";
		TypedQuery<UserEntity> query = entityManager.createQuery(jpql, UserEntity.class).setParameter("name", entity.getName()).setParameter("password", entity.getPassword());
		try {
			query.getSingleResult();
			return true;
		} catch (NoResultException e) {
			return false; 
		}
	}
	
	public void deleteUserById(Long id) throws RecordNotFoundException {
		Optional<UserEntity> user = repository.findById(id);
		
		if (user.isPresent())
			repository.delete(user.get());
		else
			throw new RecordNotFoundException();
	}
}
