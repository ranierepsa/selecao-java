package br.com.ranieresoares.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieresoares.csv.CSVHandler;
import br.com.ranieresoares.entity.CollectEntity;
import br.com.ranieresoares.exception.RecordNotFoundException;
import br.com.ranieresoares.repository.CollectRepository;

@Service
public class CollectService {

	@Autowired
	private CollectRepository repository;
	@Autowired
	private GasStationService gasStationService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<CollectEntity> getAllCollects() {
		List<CollectEntity> result = repository.findAll();
		
		if (result.size() > 0)
			return result;
		else
			return new ArrayList<CollectEntity>();
	}
	
	public CollectEntity getCollectById(Long id) throws RecordNotFoundException {
		Optional<CollectEntity> collect = repository.findById(id);
		
		if (collect.isPresent())
			return collect.get();
		else
			throw new RecordNotFoundException();
	}
	
	public CollectEntity createOrUpdateCollect(CollectEntity entity) {
		if (entity.getId() != null && repository.findById(entity.getId()).isPresent()) {
			CollectEntity newCollect = repository.findById(entity.getId()).get();
			newCollect.setProduct(entity.getProduct());
			newCollect.setCollectDate(entity.getCollectDate());
			newCollect.setBuyingPrice(entity.getBuyingPrice());
			newCollect.setSellingPrice(entity.getSellingPrice());
			newCollect.setUnit(entity.getUnit());
			newCollect.setGasStation(entity.getGasStation());
			
			gasStationService.createOrUpdateGasStation(entity.getGasStation());
			return repository.save(newCollect);
			
		} else {
			if (entity.getId() != null)
				entity.setId(null);
			
			gasStationService.createOrUpdateGasStation(entity.getGasStation());
			return repository.save(entity);
		}
	}
	
	public void deleteCollectById(Long id) throws RecordNotFoundException {
		Optional<CollectEntity> collect = repository.findById(id);
		
		if (collect.isPresent())
			repository.delete(collect.get());
		else
			throw new RecordNotFoundException();
	}
	
	public void importFile() {
		System.out.println("Importando arquivo 2018-1_CA.csv ...");
		try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/2018-1_CA.csv"))) {
			String line = br.readLine();
			long count = 0;
			while ((line = br.readLine()) != null) {
				System.out.println("Lines imported: " + count++);
				String[] values = line.split("  ");
				if (values.length > 11) continue;
				CollectEntity collect = CSVHandler.buildCollectEntity(values);
				createOrUpdateCollect(collect);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("Arquivo Importado com sucesso");
	}
	
	public List<CollectEntity> getCollectsByRegion(String region) {
		String jpql = ""
				+ " SELECT collect "
				+ " FROM CollectEntity collect "
				+ " JOIN collect.gasStation gasStation "
				+ " JOIN gasStation.address address "
				+ " WHERE address.region = :region";
		TypedQuery<CollectEntity> query = entityManager.createQuery(jpql, CollectEntity.class).setParameter("region", region);
		return query.getResultList();
	}
	
	public List<CollectEntity> getCollectsGroupByBrand(String brand) {
		String jpql = ""
				+ " SELECT collect "
				+ " FROM CollectEntity collect "
				+ " JOIN collect.gasStation gasStation "
				+ " JOIN gasStation.address address "
				+ " WHERE UPPER(gasStation.brand) = UPPER(:brand) ";
		
		TypedQuery<CollectEntity> query = entityManager.createQuery(jpql, CollectEntity.class).setParameter("brand", brand);
		return query.getResultList();
	}
	
	public List<CollectEntity> getCollectsGroupByCollectDate(String dateString) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = (Date) formatter.parse(dateString);
		
		String jpql = ""
				+ " SELECT collect "
				+ " FROM CollectEntity collect "
				+ " JOIN collect.gasStation gasStation "
				+ " JOIN gasStation.address address "
				+ " WHERE collect.collectDate = :date ";
		
		TypedQuery<CollectEntity> query = entityManager.createQuery(jpql, CollectEntity.class).setParameter("date", date);
		return query.getResultList();
	}
	
	public Double getCityAvgSellingPrice(String city) {
		String sql = ""
				+ " SELECT ROUND(AVG(collect.sell_price) * 100) / 100 "
				+ " FROM tbl_collect collect, tbl_gas_station gasStation, tbl_address address "
				+ " WHERE collect.gas_station_id = gasStation.install AND "
				+ " gasStation.address_id = address.id AND "
				+ " UPPER(address.city) = UPPER('"+city+"')";
		
		return (Double) entityManager.createNativeQuery(sql).getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getCitiesAvgBuyingSellingPrice() {
		String sql = ""
				+ " SELECT address.city, ROUND(AVG(collect.buy_price) * 100) / 100 , ROUND(AVG(collect.sell_price) * 100) / 100 "
				+ " FROM tbl_collect collect, tbl_gas_station gasStation, tbl_address address "
				+ " WHERE collect.gas_station_id = gasStation.install AND "
				+ "       gasStation.address_id = address.id "
				+ " GROUP BY address.city ";
		
		return (List<Object>) entityManager.createNativeQuery(sql).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getBrandsAvgBuyingSellingPrice() {
		String sql = ""
				+ " SELECT gasStation.brand, ROUND(AVG(collect.buy_price) * 100) / 100 , ROUND(AVG(collect.sell_price) * 100) / 100 "
				+ " FROM tbl_collect collect, tbl_gas_station gasStation, tbl_address address "
				+ " WHERE collect.gas_station_id = gasStation.install AND "
				+ "       gasStation.address_id = address.id "
				+ " GROUP BY gasStation.brand ";
		
		return (List<Object>) entityManager.createNativeQuery(sql).getResultList();
	}
}
