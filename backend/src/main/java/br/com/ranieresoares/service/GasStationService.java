package br.com.ranieresoares.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieresoares.entity.GasStationEntity;
import br.com.ranieresoares.repository.GasStationRepository;

@Service
public class GasStationService {

	@Autowired
	private GasStationRepository repository;
	
	public List<GasStationEntity> getAllGasStations() {
		List<GasStationEntity> result = repository.findAll();
		
		if (result.size() > 0)
			return result;
		else
			return new ArrayList<GasStationEntity>();
	}
	
	public GasStationEntity getGasStationById(Integer id) {
		Optional<GasStationEntity> gasStation = repository.findById(id);
		
		if (gasStation.isPresent())
			return gasStation.get();
		else
			return null;
	}
	
	public GasStationEntity createOrUpdateGasStation(GasStationEntity entity) {
		if (repository.findById(entity.getInstall()).isPresent()) {
			GasStationEntity newGasStation = repository.findById(entity.getInstall()).get();
			newGasStation.setInstall(entity.getInstall());
			newGasStation.setResale(entity.getResale());
			newGasStation.setBrand(entity.getBrand());
			newGasStation.setAdress(entity.getAdress());
			
			newGasStation = repository.save(newGasStation);
			return newGasStation;
			
		} else {
			return repository.save(entity);
		}
	}
	
	public void deleteGasStationById(Integer id) {
		Optional<GasStationEntity> gasStation = repository.findById(id);
		
		if (gasStation.isPresent())
			repository.delete(gasStation.get());
		else
			return;
	}
}
