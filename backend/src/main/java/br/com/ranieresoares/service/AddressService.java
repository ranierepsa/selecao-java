package br.com.ranieresoares.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieresoares.entity.AddressEntity;
import br.com.ranieresoares.repository.AddressRepository;

@Service
public class AddressService {

	@Autowired
	private AddressRepository repository;
	
	public List<AddressEntity> getAllAddresses() {
		List<AddressEntity> result = repository.findAll();
		
		if (result.size() > 0)
			return result;
		else
			return new ArrayList<AddressEntity>();
	}
	
	public AddressEntity getAddressById(Long id) {
		Optional<AddressEntity> address = repository.findById(id);
		
		if (address.isPresent())
			return address.get();
		else
			return null;
	}
	
	public AddressEntity createOrUpdateAddress(AddressEntity entity) {
		if (entity.getId() != null && repository.findById(entity.getId()).isPresent()) {
			AddressEntity newAddress = repository.findById(entity.getId()).get();
			newAddress.setRegion(entity.getRegion());
			newAddress.setCountryState(entity.getCountryState());
			newAddress.setCity(entity.getCity());
			
			newAddress = repository.save(newAddress);
			return newAddress;
			
		} else {
			if (entity.getId() != null) 
				entity.setId(null);
			return repository.save(entity);
		}
	}
	
	public void deleteAddressById(Long id) {
		Optional<AddressEntity> address = repository.findById(id);
		
		if (address.isPresent())
			repository.delete(address.get());
		else
			return;
	}
}
